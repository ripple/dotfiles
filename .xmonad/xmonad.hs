import XMonad
import XMonad.Util.SpawnOnce
import XMonad.Layout.Spacing
import XMonad.Layout.Gaps
import XMonad.Hooks.ManageDocks
import XMonad.Util.Run
import XMonad.Hooks.DynamicLog
import qualified XMonad.StackSet as W

-- Startup Hook
startHook = do
    spawnOnce "/home/nymk/.screenlayout/layout.sh"
    spawnOnce "sleep 0.2"
    spawnOnce "sh ~/.fehbg"
    spawnOnce "compton -f -b"
    spawnOnce "xsetroot -cursor_name left_ptr"

-- layout
layout = gaps [(U,2), (R,2), (D,2), (L,2) ] $ spacing 2 $ avoidStruts tiled ||| Mirror tiled ||| Full
    where
        tiled    = Tall nmaster delta ratio
        nmaster  = 1
        ratio    = 1/2
        delta    = 3/100

-- colours
myppCurrent = "#81a1c1"
myppVisible = "#5e81ac"
myppHidden = "#268bd2"
myppHiddenNoWindows = "#93A1A1"
myppTitle = "#FDF6E3"
myppUrgent = "#DC322F"
myWorkspaces = ["1","2","3","4","5","6","7","8","9"]

-- borders
normc  = "#2e3440"
focusc = "#434c5e"

-- Main func
main = do 
  xmproc0 <- spawnPipe "xmobar -x 0"
  xmproc1 <- spawnPipe "xmobar -x 1"

  xmonad $ docks def
      {
          -- Terminal
          terminal = "alacritty"

          -- modMask
          , modMask = mod4Mask

          -- borderWidth
          , borderWidth = 1

          -- startupHook
          , startupHook = startHook

         -- layoutHook
        , layoutHook = layout

         -- logHook
        , logHook = dynamicLogWithPP xmobarPP 
        {
            ppOutput = \x -> hPutStrLn xmproc0 x
                          >> hPutStrLn xmproc1 x  
          , ppCurrent = xmobarColor myppCurrent "" . wrap "[" "]" -- Current workspace in xmobar
                        , ppVisible = xmobarColor myppVisible ""                -- Visible but not current workspace
                        , ppHidden = xmobarColor myppHidden "" . wrap "+" ""   -- Hidden workspaces in xmobar
                        , ppHiddenNoWindows = xmobarColor  myppHiddenNoWindows ""        -- Hidden workspaces (no windows)
                        , ppTitle = xmobarColor  myppTitle "" . shorten 80     -- Title of active window in xmobar
                        , ppSep =  "<fc=#586E75> | </fc>"                     -- Separators in xmobar
                        , ppUrgent = xmobarColor  myppUrgent "" . wrap "!" "!"  -- Urgent workspace
                        , ppOrder  = \(ws:l:t:ex) -> [ws,l]++ex++[t]
        }

        -- normalBorderColor
        , normalBorderColor = normc

        -- focusedBorderColor
        , focusedBorderColor = focusc

    }
