# Prompt
autoload -U colors && colors
PROMPT="%F{yellow}@%f%F{cyan}%n%f%u:%~%# "
RPROMPT="%T|%?"

export SAVEHIST=1000000000
export HISTFILE=~/.zsh/zsh_history
export HISTFILESIZE=1000000000
export HISTSIZE=1000000000
setopt appendhistory

# FreeDesktop
XDG_DATA_HOME=$HOME/.local/share
XDG_CONFIG_HOME=$HOME/.config
XDG_STATE_HOME=$HOME/.local/state
XDG_CACHE_HOME=$HOME/.cache

# XDG-NINJA
export ANDROID_HOME="$XDG_DATA_HOME"/android
export ATOM_HOME="$XDG_DATA_HOME"/atom
export HISTFILE="${XDG_STATE_HOME}"/bash/history
export GHCUP_USE_XDG_DIRS=true
export GNUPGHOME="$XDG_DATA_HOME"/gnupg
export GTK2_RC_FILES="$XDG_CONFIG_HOME"/gtk-2.0/gtkrc
alias mbsync=mbsync -c "$XDG_CONFIG_HOME"/isync/mbsyncrc
export LESSHISTFILE="$XDG_CACHE_HOME"/less/history
export NODE_REPL_HISTORY="$XDG_DATA_HOME"/node_repl_history
export NVM_DIR="$XDG_DATA_HOME"/nvm
export _JAVA_OPTIONS=-Djava.util.prefs.userRoot="$XDG_CONFIG_HOME"/java
export RUSTUP_HOME="$XDG_DATA_HOME"/rustup
alias svn="svn --config-dir $XDG_CONFIG_HOME/subversion"
alias wget=wget --hsts-file="$XDG_DATA_HOME/wget-hsts"

# alias
alias ls="exa -la"
alias vi="kak"
alias rm="rm -r -f"
alias sudoedit="doas kak"
alias sudo="doas"
alias flatrun="flatpak run"
alias moai="git --git-dir=$HOME/moaifiles --work-tree=$HOME"
alias clock="tty-clock -c -b -C 4 -B "

# Homebrew
export PATH="/home/nymk/.deno/bin/:$HOME/.local/bin:$HOME/.cargo/bin:$HOME/.emacs.d/bin:/home/linuxbrew/.linuxbrew/bin:$HOME/.nimble/bin:$HOME/.cask/bin:$PATH"

# Autosuggestions
source ~/.zsh/zsh-autosuggestions/zsh-autosuggestions.zsh

# Syntax highlighting
source ~/.zsh/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
alias dot='/usr/bin/git --git-dir=/home/nymk/dotfiles --work-tree=/home/nymk'

[ -f "/home/nymk/.local/share/ghcup/env" ] && source "/home/nymk/.local/share/ghcup/env" # ghcup-env