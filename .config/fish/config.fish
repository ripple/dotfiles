if status is-interactive
    # Commands to run in interactive sessions can go here
end
function nvm
    bass source ~/.nvm/nvm.sh --no-use ';' nvm $argv
end
alias obs="flatpak run com.obsproject.Studio"
alias rm="rm -rf"
alias vi="nvim"
alias ls="exa -la"
alias lf="lf_p"
alias sudo="doas"
alias sudoedit="doas vim"
alias moai="git --git-dir=$HOME/moaifiles --work-tree=$HOME"
set fish_greeting
fish_add_path -ga /home/nymk/.cask/bin/
set -ga fish_user_paths /home/nymk/.nimble/bin
set -ga fish_user_paths /home/nymk/.local/bin
fish_add_path -ga /home/linuxbrew/.linuxbrew/bin
starship init fish | source
fish_add_path -ga /home/nymk/.cargo/bin
fish_add_path -ga /home/nymk/.cargo/bin
fish_add_path -ga /home/nymk/.emacs.d/bin/

set -q GHCUP_INSTALL_BASE_PREFIX[1]; or set GHCUP_INSTALL_BASE_PREFIX $HOME ; set -gx PATH $HOME/.cabal/bin $PATH /home/nymk/.ghcup/bin # ghcup-env
