;;; Nymk config
;; Files
(setq auto-save-file-name-transforms
  `((".*" "~/.cache/emacs/" t)))
(setq backup-directory-alist
  '(("" . "~/.cache/emacs/")))

;; look and feel
(tool-bar-mode -1)
(menu-bar-mode -1)
(scroll-bar-mode -1)
(setq use-dialog-box nil)
(global-display-line-numbers-mode )
(setq inhibit-startup-screen t)

;; use-package
(straight-use-package 'use-package)

;; gcmh
(use-package gcmh
  :straight t
  :init
  (gcmh-mode 1))

;; theme
(add-to-list 'custom-theme-load-path "~/.emacs.d/everforest-theme")
(load-theme 'everforest-hard-dark t)

;; Font
(set-face-attribute 'default nil :font "JetBrainsMono NF 9")

;; Evil mode
(use-package evil
  :straight t
  :init
  (evil-mode 1))

;; Mood-line
(use-package mood-line
  :straight t
  :init
  (mood-line-mode))

;; Errors
(setq warning-suppress-log-types '((comp)))

;; Minions
(use-package minions
  :straight t
  :init
  (minions-mode 1))

;; Vertico
(use-package vertico
  :straight t
  :init
  (vertico-mode)

  :config
  ;; Show more candidates
  (setq vertico-count 6)
  )

;; Dummyparens
(use-package dummyparens
  :straight t
)
(global-dummyparens-mode)

;; lsp
(use-package lsp-mode
  :defer t
  :straight t
  :init
  (add-hook 'python-mode-hook #'lsp)
  (add-hook 'rust-mode-hook #'lsp)
  (add-hook 'c++-mode-hook #'lsp)
  (add-hook 'js2-mode-hook #'lsp)
  (add-hook 'web-mode-hook #'lsp)
  :hook
  (lsp-mode . lsp-enable-which-key-integration)
  )

;; Which-key
(use-package which-key
  :straight t)

;; Tree-sitter
(use-package tree-sitter
  :straight t
  :defer t
  :config
  (tree-sitter-require 'rust)
  (tree-sitter-require 'cpp)
  (tree-sitter-require 'python)
  (tree-sitter-require 'javascript)
  (tree-sitter-require 'html)
  :init
  (add-hook 'rust-mode-hook #'tree-sitter-mode)
  (add-hook 'rust-mode-hook #'tree-sitter-hl-mode)
  
  (add-hook 'python-mode-hook #'tree-sitter-mode)
  (add-hook 'python-mode-hook #'tree-sitter-hl-mode)
  
  (add-hook 'c++-mode-hook #'tree-sitter-mode)
  (add-hook 'c++-mode-hook #'tree-sitter-hl-mode)

  (add-hook 'js2-mode-hook #'tree-sitter-mode)
  (add-hook 'js2-mode-hook #'tree-sitter-hl-mode)

  (add-hook 'web-mode-hook #'tree-sitter-mode)
  (add-hook 'web-mode-hook #'tree-sitter-hl-mode)
  )

;; Tree-sitter-langs
(use-package tree-sitter-langs
  :straight t
  :defer t
  :after tree-sitter
  )

;; Flycheck
(use-package flycheck
  :after eglot
  :straight t)

;; Company
(use-package company
  :straight t
  :init
  (global-company-mode))

;; Company box
(use-package company-box
  :straight t
  :config
  (setq company-box-scrollbar nil)
  (setq company-idle-delay 0)
  :after company)

;; Dashboard
(use-package dashboard
  :straight t
  :config
  (setq dashboard-center-content t)
  (setq dashboard-show-shortcuts nil)
  (setq dashboard-startup-banner 'logo)
  (setq dashboard-items '((recents  . 5)))
  :init
  (dashboard-setup-startup-hook))

;; Rust 
(use-package rust-mode
  :straight t)
(autoload 'rust-mode "rust-mode" nil t)
(add-to-list 'auto-mode-alist '("\\.rs\\'" . rust-mode))

;; All-the-icons
(use-package all-the-icons
  :straight t)

;; Python
(use-package lsp-pyright
  :straight t
  :hook (python-mode . (lambda ()
                          (require 'lsp-pyright)
                          (lsp))))  ; or lsp-deferred
(setq warning-suppress-log-types '((comp)))

;; C++
(use-package modern-cpp-font-lock
  :straight t)
(add-hook 'c++-mode-hook #'modern-c++-font-lock-mode)

;; Org mode
(use-package org)
(add-to-list 'auto-mode-alist '("\\.org$" . org-mode))

(use-package org-modern
  :straight t)
(use-package org-bullets
  :straight t)

(add-hook 'org-mode-hook #'org-modern-mode)
(add-hook 'org-mode-hook #'org-bullets-mode)
(require 'org-tempo)
(setq org-adapt-indentation t)

;; Javascript
(use-package js2-mode
  :straight t
  :init
  (add-to-list 'auto-mode-alist '("\\.js\\'" . js2-mode))
  (add-hook 'js2-mode-hook #'js2-imenu-extras-mode)
  )

(use-package js2-refactor
  :straight t)

(use-package xref-js2
  :straight t)

(add-hook 'js2-mode-hook #'js2-refactor-mode)
(js2r-add-keybindings-with-prefix "C-c C-r")
(define-key js2-mode-map (kbd "C-k") #'js2r-kill)

;; js-mode (which js2 is based on) binds "M-." which conflicts with xref, so
;; unbind it.
(define-key js-mode-map (kbd "M-.") nil)

(add-hook 'js2-mode-hook (lambda ()
  (add-hook 'xref-backend-functions #'xref-js2-xref-backend nil t)))

;; Html
(use-package simple-httpd
  :straight t)

(use-package htmlize
  :straight t)

(add-to-list 'load-path "~/.emacs.d/impatient-mode")
(require 'impatient-mode)

(add-hook 'impatient-mode-hook (lambda () (format t "To open the website, go to http://localhost:8080/imp/")))

(use-package web-mode
  :straight t)
(add-to-list 'auto-mode-alist '("\\.phtml\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.tpl\\.php\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.[agj]sp\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.as[cp]x\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.erb\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.mustache\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.djhtml\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode))

(use-package emmet-mode
 :straight t)
(add-hook 'web-mode-hook #'emmet-mode)

;; Persist history over Emacs restarts. Vertico sorts by history position.
(use-package savehist
  :init
  (savehist-mode))

;; A few more useful configurations...
(use-package emacs
  :init
  ;; Add prompt indicator to `completing-read-multiple'.
  ;; We display [CRM<separator>], e.g., [CRM,] if the separator is a comma.
  (defun crm-indicator (args)
    (cons (format "[CRM%s] %s"
                  (replace-regexp-in-string
                   "\\`\\[.*?]\\*\\|\\[.*?]\\*\\'" ""
                   crm-separator)
                  (car args))
          (cdr args)))
  (advice-add #'completing-read-multiple :filter-args #'crm-indicator)

  ;; Do not allow the cursor in the minibuffer prompt
  (setq minibuffer-prompt-properties
        '(read-only t cursor-intangible t face minibuffer-prompt))
  (add-hook 'minibuffer-setup-hook #'cursor-intangible-mode)

  ;; Emacs 28: Hide commands in M-x which do not work in the current mode.
  ;; Vertico commands are hidden in normal buffers.
  ;; (setq read-extended-command-predicate
  ;;       #'command-completion-default-include-p)

  ;; Enable recursive minibuffers
  (setq enable-recursive-minibuffers t))

(add-to-list 'lsp-disabled-clients 'jsts-ls)
(add-to-list 'lsp-disabled-clients 'ts-ls)
(use-package neotree
  :straight t)
(global-set-key [f8] 'neotree-dir)
(setq neo-theme (if (display-graphic-p) 'icons 'arrow))
(add-hook 'neotree-mode-hook (lambda ()(display-line-numbers-mode -1)))
